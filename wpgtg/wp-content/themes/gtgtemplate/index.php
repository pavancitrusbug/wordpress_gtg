<?php
/**
 * The main template file in GTG Templates
 */

get_header(); ?>
		<section class="readmore_section" id="readmore_section">
            <div class="readmore_div">
                <div class="container">
				<?php $args = array(
						'posts_per_page'   => 5,
						'offset'           => 0,
						'orderby'          => 'date',
						'order'            => 'ASC',
						'post_type'        => 'our_educations',
						'post_status'      => 'publish',
						'suppress_filters' => true,
					);
					$posts_array = get_posts( $args );
					if (count($posts_array) > 0) {
				?>
                    <div class="row">
						<?php foreach($posts_array as $data) { ?>
                        <div class="col-md-4 col-sm-4">
                            <div class="bg-1">
								<div class="image-div">
									<img src=<?php echo get_the_post_thumbnail_url($data->ID); ?> alt="icon">
                                </div>
                                <div class="content-div-one">
                                    <p><?php echo $data->post_content; ?></p>
                                </div>
                                <div class="btn-div-read">
                                    <a href="<?php echo get_permalink($data->ID); ?>" class="btn btn-read">Read More</a>
                                </div>
                            </div>
                        </div><!-- end of col -->
						<?php } ?>                        
                    </div>
					<?php } ?>
                </div>  
            </div><!-- end of readmore_div -->    
        </section><!-- end of readmore_section -->
		
        <section class="difference_section" id="difference">
            <div class="difference_div">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 text-center">
                            <h2>We're making a <span class="border-star">difference</span></h2>
                        </div>
						<?php $onePost = get_post($post = 1); ?>
                        <div class="difference_content">
                            <div class="col-md-7 col-sm-8">
                                <h2><?php echo $onePost->post_title; ?></h2>
                                <p>
								<?php echo apply_filters('the_content', $onePost->post_content); ?></p>
                            </div>
                            <div class="col-md-5 col-sm-4">        
                                <img src="<?php echo get_the_post_thumbnail_url($post = 1); ?>" alt="difference" class="img-responsive img-right">
                            </div>
                        </div><!-- end of difference_content -->
                    </div>
                </div>  
            </div><!-- end of difference_div -->    
        </section><!-- end of difference_section -->
	
		<?php $args = array(
				'posts_per_page'   => 5,
				'offset'           => 0,
				'orderby'          => 'date',
				'order'            => 'ASC',
				'post_type'        => 'services',
				'post_status'      => 'publish',
				'suppress_filters' => true,
			);
			$service_posts = get_posts( $args );
			if (count($service_posts) > 0) {
		?>
        <section class="services_section" id="services">
            <div class="services_div">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 text-center">
                            <h2>OUR <span class="border-star">SERVICES</span></h2>
                        </div>
                        <div class="services_content clearfix">
						<?php $counter = 1;
						foreach ($service_posts as $service) { ?>
                            <div class="col-md-4 col-sm-12">
                                <div class="row">
									<?php if ($service->post_content != '') { ?>
										<div class="bg-black-img clearfix">
									<?php } ?>
                                        <div class="<?php echo 'div-'.$counter; ?>">
											<?php if ($service->post_content != '') { ?>
												<h3><?php echo $service->post_title; ?></h3>
												<p><?php echo $service->post_content; ?></p>  
												<div class="btn-div-contact">
													<a href="<?php echo get_permalink(45); ?>" class="btn btn-contact">CONTACT US</a>
												</div>
											<?php } else { ?>
												<!-- <img src="<?php // echo get_the_post_thumbnail_url($service->ID); ?>" alt="advocacy" class="img-responsive img-center p-15"> -->
											<?php } ?>
                                        </div>
									<?php if ($service->post_content != '') { ?>
										</div>
									<?php } ?>
                                </div>
                                <div class="row">
                                    <div class="div-row">
                                        <a href="<?php echo get_permalink(64); ?>"><h3><?php echo $service->post_title; ?></h3></a>
                                    </div>
                                </div>
                            </div>
						<?php $counter++; } ?>
                        </div><!-- end of services_content --> 
                    </div>
                </div>  
            </div><!-- end of services_div -->    
        </section><!-- end of services_section -->
		<?php } ?>

    <section class="donating_section" id="Donating">
        <div class="donating_div">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 text-center">
                        <h2>Help Us By <span class="border-star">Donating</span></h2>
                    </div>
					<?php
						$percentage = 0;
						if (get_settings('progress_percentage'))
						{
							$percentage = get_settings('progress_percentage');
						}
					?>
                    <div class="donating_content">
                        <div class="col-md-12 col-sm-12">
                            <?php echo do_shortcode('[give_form id="102"]'); ?>
                        </div>
                    </div><!-- end of donating_content -->
                </div>
            </div>  
        </div><!-- end of donating_div -->    
    </section><!-- end of donating_section -->
	<?php $args = array(
				'posts_per_page'   => 5,
				'offset'           => 0,
				'orderby'          => 'date',
				'order'            => 'ASC',
				'post_type'        => 'testimonials',
				'post_status'      => 'publish',
				'suppress_filters' => true,
			);
			$testimonial_posts = get_posts( $args );
			if (count($testimonial_posts) > 0) {
			$counter = 0;
			$class = '';
		?>
	<section class="testimonial_section" id="testimonial">
        <div class="testimonial_div">
            <div class="container">
                <div class="row">

                    <div class="col-md-12 col-sm-12 text-center">
                        <h2>what our <span class="border-star">client say’s</span></h2>    
                        
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <!-- Carousel indicators -->
                            <ol class="carousel-indicators">
							<?php foreach($testimonial_posts as $testimonial) { 
									$counter++;
									if ( $counter % 2 == 0 ) {
										$class = 'active';
									}
							?>
                                <li data-target="#myCarousel" data-slide-to="<?php echo $counter; ?>" class="<?php echo $class; ?>"></li>
							<?php 
								$class = '';
								} 
							?>
                            </ol>   
                            <!-- Wrapper for carousel items -->
                            <div class="carousel-inner">
							<?php foreach($testimonial_posts as $testimonial) { 
									$counter++;
									if ( $counter % 2 == 0 ) {
										$class = 'active';
									}
							?>							
                                <div class="item carousel-item <?php echo $class; ?>">
                                    <div class="img-box">
										<img src="<?php echo get_the_post_thumbnail_url($testimonial->ID); ?>" alt="user-1">
									</div>
                                    <p class="testimonial"><?php echo $testimonial->post_content; ?></p>
                                    <p class="overview"><span class="span-1">Denise Brehmer</span>
									<span class="span-2"><?php echo get_the_excerpt($testimonial->ID); ?></span></p>
                                </div>
							<?php 
								$class = '';
								} 
							?>
                            </div>
                            <!-- Carousel controls -->
                            <a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            <a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
			</div>
        </div>
    </section><!-- end of testimonial -->
	<?php } ?>
    <section class="connect_section" id="connect">
        <div class="connect_div">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 text-center">
                        <h2><span class="border-star">CONNECT</span> WITH US</h2>
                    </div>
                    <div class="connect_content">
                        <div class="col-md-12 col-sm-12">
                            <?php echo do_shortcode('[contact-form-7 id="52" title="Contact Us From"]'); ?>
                        </div>
                    </div><!-- end of difference_content -->
                </div>
            </div>  
        </div><!-- end of difference_div -->    
    </section><!-- end of difference_section -->

    <section class="address_section" id="address">
        <div class="address_div">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 text-center">
                        <h2><span class="border-bottom-1">address</span></h2>
                    </div>
                    <div class="address_content">
                     
                        <div class="row">
							<?php if (get_settings('address')) { ?>
                            <div class="col-md-4 col-sm-12 col-xs-12 img-div-icon">
                                <div class="col-md-3 col-sm-2 col-xs-2">
                                    <span class="span-1">
                                        <img src="<?php bloginfo('template_url'); ?>/images/i1.png" alt="location" class="img-responsive">
                                    </span>
                                </div>
                                <div class="col-md-9 col-sm-10 col-xs-10">
                                    <span class="span-2"><?php echo get_settings('address'); ?></span>
                                </div>
                            </div>
							<?php } if (get_settings('telephone')) { ?>
                            <div class="col-md-4 col-sm-12 col-xs-12 img-div-icon">
                                <div class="col-md-3 col-sm-2 col-xs-2">
                                    <span class="span-1">
                                        <img src="<?php bloginfo('template_url'); ?>/images/i2.png" alt="location" class="img-responsive">
                                    </span>
                                </div>
                                <div class="col-md-9 col-sm-10 col-xs-10 img-div-icon">
                                    <span class="span-2"><?php echo get_settings('telephone'); ?></span>
                                </div>
                            </div>
							<?php } if (get_settings('facebook_url')) { ?>
                            <div class="col-md-4 col-sm-12 col-xs-12 img-div-icon">
                                <div class="col-md-3 col-sm-2 col-xs-2">
                                    <span class="span-1">
                                        <img src="<?php bloginfo('template_url'); ?>/images/i3.png" alt="location" class="img-responsive">
                                    </span>
                                </div>
                                <div class="col-md-9 col-sm-10 col-xs-10">
                                    <span class="span-2"><?php echo get_settings('facebook_url'); ?></span>
                                </div>
                            </div>
							<?php } ?>
                        </div>
						<?php if (get_settings('site_email')) { ?>
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12 img-div-icon">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <span class="span-1">
                                        <img src="<?php bloginfo('template_url'); ?>/images/i4.png" alt="location" class="img-responsive">
                                    </span>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10 clerfix">
                                    <span class="span-3"><?php echo get_settings('site_email'); ?></span>
                                </div>
                            </div>
                        </div>
						<?php } ?>
                        
                    </div><!-- end of difference_content -->
                </div>
            </div>  
        </div><!-- end of difference_div -->    
    </section><!-- end of difference_section -->  
    

<?php get_footer(); ?>
