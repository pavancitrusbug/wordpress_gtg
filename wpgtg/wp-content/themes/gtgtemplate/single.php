<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<section class="services_section" id="why-choose-us">
	<div class="services_div">
		<div class="container">
			<div class="row">
				<?php
				// Start the loop.
				while ( have_posts() ) : the_post(); ?>
				<div class="services_content clearfix">
					<div class="col-md-12 col-sm-12">
						<div class="row">
							<?php 
							/*
								 * Include the post format-specific template for the content. If you want to
								 * use this in a child theme, then include a file called content-___.php
								 * (where ___ is the post format) and that will be used instead.
								 */
								get_template_part( 'content', get_post_format() );
							?>
						</div>
					</div>        
				   
				</div><!-- end of services_content -->
				<?php // End the loop.
					endwhile;
					?>

			</div>
		</div>  
	</div><!-- end of services_div -->    
</section><!-- end of services_section -->
<?php get_footer(); ?>
