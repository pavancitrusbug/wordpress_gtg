<?php
/*
 * Template Name: Why Choose Us
 *
 * @Author name: Citrusbug
 */
 
get_header(); 
$page = get_the_ID();
$page_id = get_page( $page, $output = OBJECT, $filter = 'raw');

?>
<section class="services_section" id="why-choose-us">
	<div class="services_div">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 text-center">
					<h2 class="mb-80"><span class="border-star-1">Why Choose Us</span></h2>
				</div>

				<div class="services_content clearfix">
					<div class="col-md-12 col-sm-12">
						<div class="row">
							<?php echo get_the_post_thumbnail( $post = $page, $size = 'full', $attr = '' )  ?>       
						</div><!-- row -->
					</div>
					<div class="col-md-12 col-sm-12">
						<div class="row">
							<?php echo apply_filters('the_content', $page_id->post_content); ?>
						</div>
					</div>        
				   
				</div><!-- end of services_content --> 

			</div>
		</div>  
	</div><!-- end of services_div -->    
</section><!-- end of services_section -->

<?php get_footer(); ?>
