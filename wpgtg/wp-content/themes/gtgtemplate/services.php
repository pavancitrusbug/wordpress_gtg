<?php
/*
 * Template Name: Services
 *
 * @Author name: Citrusbug
 */
 
get_header(); ?>

	<section class="services_section" id="services">
            <div class="services_div">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 text-center">
                            <h2 class="mb-80">OUR <span class="border-star">SERVICES</span></h2>
                        </div>
						<?php $args = array(
								'posts_per_page'   => 5,
								'offset'           => 0,
								'category'         => '',
								'category_name'    => '',
								'orderby'          => 'date',
								'order'            => 'ASC',
								'include'          => '',
								'exclude'          => '',
								'meta_key'         => '',
								'meta_value'       => '',
								'post_type'        => 'services',
								'post_mime_type'   => '',
								'post_parent'      => '',
								'author'	   => '',
								'author_name'	   => '',
								'post_status'      => 'publish',
								'suppress_filters' => true,
								'fields'           => '',
							);
							$service_posts = get_posts( $args );
							if (count($service_posts) > 0) {
						?>
                        <div class="services_content clearfix">
                            <div class="col-md-12 col-sm-12">
								<?php $counter = 1;
									foreach ($service_posts as $service) { 
									$setblock = $counter % 2;
								?>
                                <div class="row mb-15">
									<?php if ($setblock != 0) { ?>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="row">
                                            <div class="bg-black-img-<?php echo $counter; ?> clearfix">
                                                <div class="div-1 p-0 bg-none">
													
                                                </div>
                                            </div>
                                        </div>    
                                    </div>
									<?php } ?>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="row">
                                            <div class="clearfix">
                                                <div class="div-1 p-0 bg-black-pure">
                                                    <h3 class="h3-service"><?php echo $service->post_title; ?></h3>
                                                    <p class="p-service"><?php echo $service->post_content; ?></p>  
                                                    <div class="btn-div-contact">
                                                        <a href="<?php echo get_permalink(45); ?>" class="btn btn-contact">CONTACT US</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
									<?php if ($setblock == 0) { ?>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="row">
                                            <div class="bg-black-img clearfix">
                                                <div class="div-1 p-0 bg-none">
													
                                                </div>
                                            </div>
                                        </div>    
                                    </div>
									<?php } ?>
                                </div><!-- row -->
								<?php $counter++; } ?>
                                
                        </div><!-- end of services_content --> 
					<?php } ?>
                    </div>
                </div>  
            </div><!-- end of services_div -->    
        </section><!-- end of services_section -->

<?php get_footer(); ?>
