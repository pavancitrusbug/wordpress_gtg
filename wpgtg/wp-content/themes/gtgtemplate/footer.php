<?php
/**
 * The template for displaying the footer
 */
?>
    <section class="subscribe_section" id="connect">
        <div class="subscribe_div">
            <div class="container">
                <div class="row">
                    <div class="subscribe_content">
                        <div class="col-md-12 col-sm-12">
                            <?php echo do_shortcode('[contact-form-7 id="53" title="Subscribe form"]'); ?>
                        </div>
                    </div><!-- end of difference_content -->
                </div>
            </div>  
        </div><!-- end of difference_div -->    
    </section><!-- end of difference_section -->
	</div><!-- end of content area -->
	<footer>
    	<div class="footer-div">
        	<div class="container">
                <div class="row">
                    <div class="footer-top clearfix">
                        <div class="col-md-4 col-sm-4">
                            <h3 class="border-line-1"><?php echo get_the_title(55); ?></h3>
                            <div>
								<p><?php $about_us_page = get_post(55); 
										 echo $about_us_page->post_content; ?></p>
                            </div>
                        </div><!-- end of col -->
                        <div class="col-md-3 col-sm-3">
                            <h3 class="border-line-2">QUICK LINKS</h3>
                            <?php 
												
								$defaults = array(
								 'theme_location'  => 'primary',
								 'menu'            => 'footer-menu',
								 'container'       => 'ul',
								 'menu_class'      => 'upperLetter',
								 'echo'            => true,
								 'fallback_cb'     => 'wp_page_menu',
								 'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								 'depth'           => 0,
								 'walker' => new wp_bootstrap_navwalker
								);
								wp_nav_menu( $defaults );
								
							?>
                        </div><!-- end of col -->
                        <div class="col-md-5 col-sm-5">
                            <?php echo do_shortcode('[contact-form-7 id="54" title="SignUp Form"]'); ?>
                        </div><!-- end of col -->

                      
                    </div><!-- end of footer-top -->
                </div><!-- end of row -->
            </div><!-- end of container -->
            
        </div><!-- end of footer-div -->
    </footer>

</div><!-- end of wrapper -->

<script src="<?php bloginfo('template_url'); ?>/js/jquery.min.js" type="text/javascript"></script> 

<script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js" type="text/javascript"></script>
<link href="<?php bloginfo('template_url'); ?>/css/hover-dropdown-menu.css" rel="stylesheet" />
<script type="<?php bloginfo('template_url'); ?>/text/javascript" src="js/hover-dropdown-menu.js"></script> 

	<script src="<?php bloginfo('template_url'); ?>/js/give.all.min.js" type="text/javascript"></script>

<script>
	$(document).ready(function(){
		$("li#menu-item-76").removeClass('active');
		
		$(".donation button.amount").first().addClass('color-btn-bg-active');
		$(".amount-box").val($(".donation button.amount").val());
		$("#total_amount").html($(".donation button.amount").val());
		
		$("#online_donation").prop('checked', true);
		$("div.info-div #offline-donation-form").hide();
		
		$(".amount-box").focusout(function(){
			if(!$.isNumeric( $(this).val() )) {
				$(this).val('');
			}
			$("#total_amount").html($(this).val());
		});
	});
	
	$(document).on('click', 'li#menu-item-76', function(){
		$( "ul#menu-header-menu li" ).each(function() {
			$( this ).removeClass('active');
		});
		$('li#menu-item-76').addClass('active');
	});
	
	$(document).on('click', '.donation button.amount', function(){
		var amount = $(this).attr('data-val');
		$( ".donation button.amount" ).each(function() {
			$( this ).removeClass('color-btn-bg-active');
		});
		$(this).addClass('color-btn-bg-active');
		$(".amount-box").val($(this).val());
		$("#total_amount").html($(this).val());
	});
	
	$(document).on('click', '.custom-amount', function(){
		$( ".donation button.amount" ).each(function() {
			$( this ).removeClass('color-btn-bg-active');
		});
		$(".amount-box").val('').focus();
		$("#total_amount").html('');
	});
	
	$(document).on('change', '.radio-div input[type=radio]', function(){
		var elementId = $(this).attr('id');
		if (elementId == 'online_donation') {
			$("div.info-div #offline-donation-form").hide();
			$("div.info-div #online-donation-form").show();
		} else {
			$("div.info-div #online-donation-form").hide();
			$("div.info-div #offline-donation-form").show();
		}
	});
</script>
   
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/custom.js"></script> 
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/index.js"></script>  
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">

<?php wp_footer(); ?>

</body>
</html>
