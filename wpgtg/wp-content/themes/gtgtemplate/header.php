<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link href='images/favicon.png' rel='shortcut icon'>
	<link href='images/favicon.ico' rel='icon' type='image/ico'>
	<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	
	<link href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" media="all" rel="stylesheet" type="text/css" />
	<link href="<?php bloginfo('template_url'); ?>/css/animate.css" rel="stylesheet" />
	<script src="<?php bloginfo('template_url'); ?>/js/modernizr.js" type="text/javascript"></script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="wrapper">
	<?php if (is_front_page()) { ?>
	<section class="banner_img">
		<header>
			<div class="banner-container clearfix">
				<div class="header-div clearfix" id="header-div" >
					<div class="container">
						<div class="row">
							<div class="col-md-2 col-sm-3">
								<div class="logo-div logo-div-1">
								<?php gtg_the_custom_logo('logo-img img-responsive'); ?>
								</div>
							</div>
							<div class="col-md-10 col-sm-9">
								<div class="nav-div">
									<div class="navbar" role="navigation">
										<div class="navbar-header">
											<!-- Button For Responsive toggle -->
											<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
												<span class="sr-only">Toggle navigation</span> 
												<span class="icon-bar"></span> 
												<span class="icon-bar"></span> 
												<span class="icon-bar"></span>
											</button>
										</div>
										<!-- Navbar Collapse -->
										
										<div class="navbar-collapse collapse">
											<!-- nav -->
											<?php 
												
												$defaults = array(
												 'theme_location'  => 'primary',
												 'menu'            => 'header-menu',
												 'container'       => 'ul',
												 'menu_class'      => 'nav navbar-nav',
												 'echo'            => true,
												 'fallback_cb'     => 'wp_page_menu',
												 'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
												 'depth'           => 0,
												 'walker' => new wp_bootstrap_navwalker
												);
												wp_nav_menu( $defaults );
												
										   ?>
											<!-- end of navbar -->
										</div><!-- end of nav bar collapse -->
									</div><!-- end of nav-bar -->
								</div><!-- end of nav-div -->
							</div>	
						</div>	
					</div>
				</div><!-- end of header div -->
			</div><!-- end of banner container -->
		</header>
		
		<section class="margin-top-180">
			<div class="banner-content-div">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<h2 class="color-h2"><?php echo get_settings('homepage_line1'); ?></h2>
							<h2 class="color-white"><?php echo get_settings('homepage_line2'); ?></h2>
							<div class="btn-div">
								<a href="<?php echo get_settings('homepage_link'); ?>" class="btn btn-ready">Ready to Get Started</a>
							</div>
						</div>
						
					</div>
				</div>		
			</div>
		</section>
	</section>
	<?php } else { ?>
	<section class="service-page">
        <div class="service-div">
            <header>
                <div class="banner-container clearfix">
                    <div class="header-div header-div-all clearfix">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-2 col-sm-3">
									<div class="logo-div-all">
									<?php //echo get_logo(); ?>
										<?php gtg_the_custom_logo('logo-all img-responsive'); ?>
									</div>
                                </div>
                                <div class="col-md-10 col-sm-9">
                                    <div class="nav-div">
                                        <div class="navbar" role="navigation">
                                            <div class="navbar-header">
                                                <!-- Button For Responsive toggle -->
                                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                                    <span class="sr-only">Toggle navigation</span> 
                                                    <span class="icon-bar"></span> 
                                                    <span class="icon-bar"></span> 
                                                    <span class="icon-bar"></span>
                                                </button>
                                            </div>
                                            <!-- Navbar Collapse -->
                                            
                                            <div class="navbar-collapse collapse">
												<!-- nav -->
												<?php 
													
													$defaults = array(
													 'theme_location'  => 'primary',
													 'menu'            => 'header-menu',
													 'container'       => 'ul',
													 'menu_class'      => 'nav navbar-nav',
													 'echo'            => true,
													 'fallback_cb'     => 'wp_page_menu',
													 'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
													 'depth'           => 0,
													 'walker' => new wp_bootstrap_navwalker
													);
													wp_nav_menu( $defaults );
													
												?>
												<!-- end of navbar -->
                                            </div><!-- end of nav bar collapse -->
                                        </div><!-- end of nav-bar -->
                                    </div><!-- end of nav-div -->
                                </div>	
                            </div>	
                        </div>
                    </div><!-- end of header div -->
                </div><!-- end of banner container -->
            </header>
            
            <section class="margin-top-150">
                <div class="banner-content-div">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                            </div>
                        </div>
                    </div>		
                </div>
            </section>
		</div>
	</section>
	<?php } ?>
    <div class="content-area" id="content-div">
