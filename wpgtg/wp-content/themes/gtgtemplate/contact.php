<?php
/*
 * Template Name: Contact Us
 *
 * @Author name: Citrusbug
 */
 
get_header(); ?>

	<section class="services_section" id="contact-us">
            <div class="services_div">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 text-center">
                            <h2 class="mb-80"><span class="border-star-1">Contact Us</span></h2>
                        </div>
                        <div class="services_content clearfix">
                            <div class="col-md-12 col-sm-12">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="row">
                                            <div class="clearfix">
                                                <!--<div id="googleMap" class="map-div"></div>

                                                <script>
                                                function myMap() {
                                                var mapProp= {
                                                    center:new google.maps.LatLng(41.0825918,-85.2209395),
                                                    zoom:5,
                                                };
                                                var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
                                                }
                                                </script>

                                                <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu-916DdpKAjTmJNIgngS6HL_kDIKU0aU&callback=myMap"></script>-->
                                                <div id="map"></div>
                                                <script>
												  function initMap() {
												  
													var myLatLng = {lat: 41.0825918, lng: -85.2377636};

													var map = new google.maps.Map(document.getElementById('map'), {
													  zoom: 6,
													  center: myLatLng
													});

													var marker = new google.maps.Marker({
													  position: myLatLng,
													  map: map,
													  title: 'map'
													});
												  }
												  
												</script>
												<script async defer
												src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpiWWaPaKhBbPSs2vZBECcZcvH2LUtj2U&callback=initMap">
												</script>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-md-6 col-sm-12">
                                        <section class="connect_section" id="contact">
                                            <div class="connect_div contact-us-div">
                                                <div class="">
                                                    <div class="row">
                                                        <div class="connect_content">
                                                            <div class="col-md-12 col-sm-12">
                                                                <!-- <h2 class="mb-20 color-con">Contact Info</h2> -->
																<?php if (get_settings('address')) { ?>
																	<p class="font-auto">
																	<b class="b-space">Address:</b>
																	<?php echo get_settings('address'); ?></p>
																<?php } if (get_settings('telephone')) { ?>
																	<p class="font-auto">
																	<b class="b-space">Telephone:</b>
																	<?php echo get_settings('telephone'); ?></p>
																<?php } if (get_settings('site_email')) { ?>
																	<p class="font-auto">
																	<b class="b-space">Email:</b>
																	<?php echo get_settings('site_email'); ?></p>
																<?php } if (get_settings('facebook_url')) { ?>
																	<p class="font-auto">
																	<b class="b-space">Follow us:</b>
																	<?php echo get_settings('facebook_url'); ?></p>
																<?php } ?>
                                                            </div>
                                                            <div class="col-md-12 col-sm-12">
                                                                <?php echo do_shortcode('[contact-form-7 id="52" title="Contact Us From"]'); ?>
                                                            </div>
                                                        </div><!-- end of difference_content -->
                                                    </div>
                                                </div>  
                                            </div><!-- end of difference_div -->    
                                        </section><!-- end of difference_section -->
                                    </div>
                                </div><!-- row -->
                            </div>
                        </div><!-- end of services_content --> 
                    </div>
                </div>  
            </div><!-- end of services_div -->    
        </section><!-- end of services_section -->

<?php get_footer(); ?>
