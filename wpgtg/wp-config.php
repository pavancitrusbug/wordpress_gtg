<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress_gtg');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/^e&U*Y8x!]>d+[j(#Fv)oa-JPrzUJYX*=! yjWD48&Ae`~sDE yo~VZ#$MQKcX<');
define('SECURE_AUTH_KEY',  'x7Q~2^X):kzD,%qGAtIf.P~w-0Nr;1xv]HJSsrP9u4Bn1FrJcGZd|Jw%&h}NSC$Z');
define('LOGGED_IN_KEY',    'y?WR#&!TyKYNJ6B)WXn2gC4>uMzI+]hag>Eq+Q|(y.~P`){`SmB9}YB>69mw|*nG');
define('NONCE_KEY',        'aMp&+JGav_?n(O2^<XxX(9@?XK?:{3[89~8Yq^%hn^Fudh.9LRo$1(tKR(6Ci|V{');
define('AUTH_SALT',        '++j3%]hpef jIiH7Cw9[i=#B. gQsmZ$@bfLh)Oy4hAH>SgKuG*RxHPu9,{=@<)f');
define('SECURE_AUTH_SALT', 'Bu/B.0[>^|82-~BU2^v5CSg( ]}F[&y|5Un+dQgocesoniD]!v&F2_F;?%Y$#9kv');
define('LOGGED_IN_SALT',   ')$e7g^L&$9uW7<F_?S;B!:%>!m6qy2{vaC RJpH1|1qMwvZyf@ByE/K`>QzS%bwd');
define('NONCE_SALT',       'uYu-GmG_ALdoPEv*ELp6uP2I-c~#$D0ze:uT{QT{(0uEs:[,%^aCK5yfh5gKxp w');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
